! This is part of the netCDF package.
! Copyright 2006 University Corporation for Atmospheric Research/Unidata.
! See COPYRIGHT file for conditions of use.

! This is an example which reads some surface pressure and
! temperatures. The data file read by this program is produced
! comapnion program sfc_pres_temp_wr.f90. It is intended to illustrate
! the use of the netCDF fortran 90 API.

! This program is part of the netCDF tutorial:
! http://www.unidata.ucar.edu/software/netcdf/docs/netcdf-tutorial

! Full documentation of the netCDF Fortran 90 API can be found at:
! http://www.unidata.ucar.edu/software/netcdf/docs/netcdf-f90

! $Id: sfc_pres_temp_rd.f90,v 1.7 2006/12/09 18:44:58 russ Exp $

! gfortran -o run sfc_pres_temp_rd.f90  `nf-config --fflags --flibs`
! ./run

program sfc_pres_temp_rd
  use netcdf
  implicit none

  ! This is the name of the data file we will read.
  character (len = *), parameter :: FILE_NAME = "hysplit2020x01x03x19x25x45.0345Nx068.6821Wx00107.nc"
  integer :: ncid

  ! This is the name of the data file we will read.
  character (len = *), parameter :: FILE_NAME2 = "sum_hysplit2020x01x03x19x25x45.0345Nx068.6821Wx00107.nc"
  integer :: ncid2

  ! We are reading 2D data, a 6 x 12 lat-lon grid.
  ! sergio: y si no sabemos?
  integer, parameter :: NDIMS = 240
  integer, parameter :: NDIMS2 = 2
  integer, parameter :: NLATS = 70, NLONS = 120
  character (len = *), parameter :: LAT_NAME = "foot1lat"
  character (len = *), parameter :: LON_NAME = "foot1lon"
  integer :: lat_dimid, lon_dimid

  ! For the lat lon coordinate netCDF variables.
  real :: lats(NLATS), lons(NLONS)
  integer :: lat_varid, lon_varid

  ! We will read foot1 field. 
  character (len = *), parameter :: PRES_NAME = "foot1"
  integer :: pres_varid
  integer :: dimids(NDIMS)

  ! We will write sumfoot1 field. 
  character (len = *), parameter :: sumfoot1="sumfoot1"
  integer :: sumfoot1_varid
  integer :: dimids2(NDIMS2)

  ! To check the units attributes.
  character (len = *), parameter :: UNITS = "units"
  character (len = *), parameter :: PRES_UNITS = "ppm per (micromol m-2 s-1)"
  character (len = *), parameter :: LAT_UNITS = "degrees_north"
  character (len = *), parameter :: LON_UNITS = "degrees_east"
  integer, parameter :: MAX_ATT_LEN = 80
  integer :: att_len
  character*(MAX_ATT_LEN) :: pres_units_in
  character*(MAX_ATT_LEN) :: lat_units_in, lon_units_in

  ! Read the data into the array.
  real :: pres_in(NLONS, NLATS, NDIMS)
  real :: foot1sum(NLONS, NLATS)

  ! We will learn about the data file and store results in these
  ! program variables.
  integer :: ndims_in, nvars_in, ngatts_in, unlimdimid_in

  ! Loop indices
  integer :: lat, lon

  ! Open the file. 
  call check( nf90_open(FILE_NAME, nf90_nowrite, ncid) )

  ! There are a number of inquiry functions in netCDF which can be
  ! used to learn about an unknown netCDF file. 
  ! NF90_INQ tells how many
  ! netCDF variables, dimensions, and global attributes are in the
  ! file; 
  ! also the dimension id of the unlimited dimension, if there
  ! is one.
  call check( nf90_inquire(ncid, ndims_in, nvars_in, ngatts_in, unlimdimid_in) )

  ! Get the varids of the latitude and longitude coordinate variables.
  call check( nf90_inq_varid(ncid, LAT_NAME, lat_varid) )
  call check( nf90_inq_varid(ncid, LON_NAME, lon_varid) )

  ! Read the latitude and longitude data.
  call check( nf90_get_var(ncid, lat_varid, lats) )
  call check( nf90_get_var(ncid, lon_varid, lons) )


  ! Get the varids of the pressure and temperature netCDF variables.
  call check( nf90_inq_varid(ncid, PRES_NAME, pres_varid) )

  ! Read the surface foot1 data from the file.
  ! Since we know the contents of the file we know that the data
  ! arrays in this program are the correct size to hold all the data.
  call check( nf90_get_var(ncid, pres_varid, pres_in) )

  ! Check the data. It should be the same as the data we wrote.
  do lon = 1, NLONS
     do lat = 1, NLATS
     foot1sum(lon, lat) = sum(pres_in(lon, lat, :))
     end do
  end do
     print *, "sum of foot1: ", sum(foot1sum)

  ! Each of the netCDF variables has a "units" attribute.
  ! Let's read them and check them.
  call check( nf90_get_att(ncid, lat_varid, UNITS, lat_units_in) )
  call check( nf90_inquire_attribute(ncid, lat_varid, UNITS, len = att_len) )
  if (lat_units_in(1:att_len) /= LAT_UNITS) stop 2

  call check( nf90_get_att(ncid, lon_varid, UNITS, lon_units_in) )
  call check( nf90_inquire_attribute(ncid, lon_varid, UNITS, len = att_len) )
  if (lon_units_in(1:att_len) /= LON_UNITS) stop 2

  call check( nf90_get_att(ncid, pres_varid, UNITS, pres_units_in) )
  call check( nf90_inquire_attribute(ncid, pres_varid, UNITS, len = att_len) )
  if (pres_units_in(1:att_len) /= PRES_UNITS) stop 2
  ! Close the file. This frees up any internal netCDF resources
  ! associated with the file.
  call check( nf90_close(ncid) )


  ! Create the file. 
  call check( nf90_create(FILE_NAME2, nf90_clobber, ncid2) )

  ! Define the dimensions.
  call check( nf90_def_dim(ncid2, LAT_NAME, NLATS, lat_dimid) )
  call check( nf90_def_dim(ncid2, LON_NAME, NLONS, lon_dimid) )
  ! Define the coordinate variables. 
  ! They will hold the coordinate
  ! information, that is, the latitudes and longitudes. A varid is
  ! returned for each.
  call check( nf90_def_var(ncid2, LAT_NAME, NF90_REAL, lat_dimid, lat_varid) )
  call check( nf90_def_var(ncid2, LON_NAME, NF90_REAL, lon_dimid, lon_varid) )
 ! Assign units attributes to coordinate var data. 
  ! This attaches a
  ! text attribute to each of the coordinate variables, containing the
  ! units.
  call check( nf90_put_att(ncid2, lat_varid, UNITS, LAT_UNITS) )
  call check( nf90_put_att(ncid2, lon_varid, UNITS, LON_UNITS) )
  ! Define the netCDF variables. 
  ! The dimids2 array is used to pass the
  ! dimids2 of the dimensions of the netCDF variables.
  dimids2 = (/ lon_dimid, lat_dimid /)
  call check( nf90_def_var(ncid2, sumfoot1, NF90_REAL, dimids2, sumfoot1_varid) )
  ! Assign units attributes to the sumfoot1 netCDF
  call check( nf90_put_att(ncid2, sumfoot1_varid, UNITS, PRES_UNITS) )
  ! End define mode.
  call check( nf90_enddef(ncid2) )
  ! Write the pretend data. 
  call check( nf90_put_var(ncid2, sumfoot1_varid, foot1sum) )
  ! Close the file.
  call check( nf90_close(ncid2) )


  ! If we got this far, everything worked as expected. Yipee! 
  print *,"*** SUCCESS writing ", FILE_NAME2

contains
  subroutine check(status)
    integer, intent ( in) :: status
    
    if(status /= nf90_noerr) then 
      print *, trim(nf90_strerror(status))
      stop "Stopped"
    end if
  end subroutine check  
end program sfc_pres_temp_rd
